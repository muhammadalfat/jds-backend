from sqlalchemy import (
    Column,
    Integer,
    MetaData,
    String,
    Table,
)
from sqlalchemy.sql.sqltypes import Date
from decouple import config
from sqlalchemy.sql import text
from config.database import conn

metadata = MetaData(schema=config('DATABASE_SCHEMA'))
SaranaUnitMobil = Table(
    "tb_h_jumlah_sarana_unit_mobil", metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("kode_provinsi", Integer),
    Column("nama_provinsi", String(512)),
    Column("bps_kode_kabupaten_kota", Integer),
    Column("bps_nama_kabupaten_kota", String(512)),
    Column("jenis_kendaraan", String(512)),
    Column("kendaraan", String(512)),
    Column("jumlah_mobil", String(50)),
    Column("satuan", String(50)),
    Column("tahun", String(5)),
    Column("created_by", String(50)),
    Column("created_dt", Date()),
    Column("updated_by", String(50)),
    Column("updated_dt", Date()),
)


async def get_jumlah_jenis_kendaraan() :
    # Step 1: Drop the temporary table if it exists
    # Step 2: Create the temporary table and insert the aggregated data into it
    # Step 3: Select from the temporary table
    query = """
        DROP TABLE IF EXISTS TB_T_JUMLAH_MOBIL;

        CREATE TEMP TABLE TB_T_JUMLAH_MOBIL AS
        SELECT jenis_kendaraan,
            SUM(CASE 
                    WHEN jumlah_mobil ~ '^\d+$' THEN jumlah_mobil::int 
                    ELSE 0 
                END) AS total_jumlah_mobil
        FROM tb_h_jumlah_sarana_unit_mobil
        GROUP BY jenis_kendaraan;

        SELECT 
            jenis_kendaraan, 
            total_jumlah_mobil
        FROM TB_T_JUMLAH_MOBIL
        ORDER BY total_jumlah_mobil;
    """

    query_text = text(query)
    data = conn.execute(query_text).fetchall()

    data = [dict(row) for row in data]

    return data



async def get_jumlah_jenis_kendaraan_per_tahun() :
    # Step 1: Drop the temporary table if it exists
    # Step 2: Create the temporary table and insert the aggregated data into it
    # Step 3: Select from the temporary table
    query = """
        DROP TABLE IF EXISTS TB_T_JUMLAH_MOBIL_PER_TAHUN;

        CREATE TEMP TABLE TB_T_JUMLAH_MOBIL_PER_TAHUN AS
        SELECT tahun,
            SUM(CASE 
                    WHEN jumlah_mobil ~ '^\d+$' THEN jumlah_mobil::int 
                    ELSE 0 
                END) AS total_jumlah_mobil
        FROM tb_h_jumlah_sarana_unit_mobil
        GROUP BY tahun;

        SELECT 
            tahun, 
            total_jumlah_mobil
        FROM TB_T_JUMLAH_MOBIL_PER_TAHUN
        ORDER BY tahun;
    """

    query_text = text(query)
    data = conn.execute(query_text).fetchall()

    data = [dict(row) for row in data]

    return data



async def get_jumlah_detail_jenis_kendaraan_per_tahun() :
    # Step 1: Drop the temporary table if it exists
    # Step 2: Create the temporary table and insert the aggregated data into it
    # Step 3: Select from the temporary table
    query = """
        DROP TABLE IF EXISTS TB_T_JUMLAH_DETAIL_MOBIL_TAHUN;

        CREATE TEMP TABLE TB_T_JUMLAH_DETAIL_MOBIL_TAHUN AS
        SELECT DISTINCT t1.jenis_kendaraan, t2.tahun
        FROM (SELECT DISTINCT jenis_kendaraan FROM tb_h_jumlah_sarana_unit_mobil) t1
        CROSS JOIN (SELECT DISTINCT tahun FROM tb_h_jumlah_sarana_unit_mobil) t2;

        SELECT 
            jmt.jenis_kendaraan, 
            jmt.tahun,
            SUM(CASE 
                    WHEN jsum.jumlah_mobil ~ '^\d+$' THEN jsum.jumlah_mobil::int 
                    ELSE 0 
                END) AS total_jumlah_mobil
        FROM TB_T_JUMLAH_DETAIL_MOBIL_TAHUN jmt
        LEFT JOIN tb_h_jumlah_sarana_unit_mobil jsum 
            ON jmt.jenis_kendaraan = jsum.jenis_kendaraan 
            AND jmt.tahun = jsum.tahun
        GROUP BY 
            jmt.jenis_kendaraan, 
            jmt.tahun
        ORDER BY tahun, jenis_kendaraan;
    """

    query_text = text(query)
    data = conn.execute(query_text).fetchall()

    data = [dict(row) for row in data]

    return data
