from fastapi.responses import JSONResponse
from datetime import datetime

def Ok(message, data = None):
    return create_response(200, data, message)

def SearchOk(message, data = None):
    return create_response(200, data[0], message, paging_info=data[1])

def NotFound(message):
    return create_response(404, None, message)

def BadRequest(message):
    return create_response(400, None, message, error="Bad Request")

def Unauthorized(message):
    return create_response(401, None, message, error="Unauthorized")

def Forbidden(message):
    return create_response(403, None, message, error="Forbidden")

def InternalServerError(do, exception = None):
    if exception is not None:
        now = datetime.now()
        date_str = now.strftime("%Y-%m-%d %H:%M:%S")
        print(f"------> Error occured at {date_str}")
        print(exception, end="\n\n")

    message = f"Error while {do}"
    return create_response(500, None, message, error="Internal Server Error")

def create_response(status_code, data, message = None, error = None, paging_info = None) :
    if data is not None and hasattr(data, "__len__"):
        data = None if len(data) == 0 else data
        
    content = {
        "status_code": status_code,
        "message": message,
    }

    if not (data is None and status_code != 404):
        content["data"] = data

    if error is not None:
        content["error"] = error

    if paging_info is not None:
        content["paging_info"] = paging_info

    return JSONResponse(content=content, status_code=status_code)
