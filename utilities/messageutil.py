response_message = {
  "login_failed": "Username or password is incorrect",
  "login_success": "Login success",
  "logout_success": "Logout success",
  "reset_password_success": "New password has been sent to user's email",

  "data_found": "Data found",
  "data_not_found": "Data not found",

  "upload_file_success": "Successfully uploaded file",

  "file_found": "File found",
  "file_not_found": "File not found",
  "image_not_found": "Image not found, please upload first",


  "not_authenticated": "Token is missing",
  "invalid_authorization": "Invalid authentication scheme",
  "invalid_token": "Token is invalid",
  "expired_token": "Your session was end, please re-login",
  "multiple_login": "It looks like you are already logged in to another tab or browser. To log in, please close or log out of any other tabs or windows where you are logged in",

  "send_otp": "OTP has been sent",
  "incorrect_otp": "OTP is incorrect",
  "expired_otp": "OTP is expired",
  "verified_otp": "OTP has been verified",
  "forgot_password_success": "Password has been changed",
  "change_password_success": "Password has been changed",

  "confirm_password_not_match": "Confirm password and password is not match",
  "password_not_match": "Current password is not match",

  "duplicat_username": "Username has been taken",
  "duplicat_email": "Email has been taken",
  "duplicat_name": "Name has been taken",
  "duplicat_system": "System data has been taken",

  "data_created": "Data successfully created",
  "user_created": "User created. The password has been sent to user's email",
  "data_updated": "Data successfully updated",
  "data_deleted": "Data successfully deleted",


  "invalid_role": "Role is invalid",


  "parent_menu_not_found": "Parent menu not found",
  "id_is_equals_to_parent_menu": "Menu ID and parent menu ID cannot be same",
  "system_key_is_same": "Category, sub category, and code cannot be same",


  "menu_have_children": "Can't delete parent menu that has children menu",
  "role_have_users": "Can't delete role that has users",
  "id_or_name_is_required": "ID or Name must be not empty",
  "id_or_username_is_required": "ID or Username must be not empty",
}