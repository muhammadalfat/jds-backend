How to run
1. Create python env scope and activate (optional)
  > python -m venv [env_name]
  > [path/to/env_name]/Scripts/activate.bat (windows/cmd)
2. Install required packages from requirements_win.txt 
  > pip install -r requirements_win.txt
3. Run project
  > uvicorn main:app --port 8085 --reload
4. Backup database
  > ./database/backup.sh (Git Bash)

Swagger
1. Open browser. Link: http://localhost:8085/swagger

Apps
1. Python 3.12.0
2. Postgres


Build app :
- pyinstaller main.py

Run from dist :
- ./dist/main/main