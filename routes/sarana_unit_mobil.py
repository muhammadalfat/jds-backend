from fastapi import APIRouter, Depends, Request

from fastapi.security.api_key import APIKeyHeader, APIKey
from middleware.auth_api_key import verify_api_key
from utilities.responseutil import Ok, InternalServerError, BadRequest, NotFound, SearchOk
from utilities.messageutil import response_message
from models.sarana_unit_mobil import get_jumlah_jenis_kendaraan, get_jumlah_jenis_kendaraan_per_tahun, get_jumlah_detail_jenis_kendaraan_per_tahun

from schemas.sarana_unit_mobil import QueryParams, ReturnType

from collections import defaultdict

# user = APIRouter(dependencies=[Depends(JWTBearer())])
sarana_unit_mobil = APIRouter()

@sarana_unit_mobil.get('/jumlah_jenis_kendaaran', description="Search jumlah jenis kendaaran")
async def jumlah_jenis_kendaaran(api_key: APIKey = Depends(verify_api_key)):
    try :
        data = await get_jumlah_jenis_kendaraan()
        if data is None or len(data[0]) == 0:
            return NotFound(response_message["data_not_found"])

        return Ok(response_message["data_found"], data)
    except Exception as e:
        return InternalServerError("searching jumlah_jenis_kendaaran", e)


@sarana_unit_mobil.get('/jumlah_jenis_kendaaran_per_tahun', description="Search jumlah jenis kendaaran per tahun")
async def jumlah_jenis_kendaaran_per_tahun(api_key: APIKey = Depends(verify_api_key)):
    try :
        data = await get_jumlah_jenis_kendaraan_per_tahun()
        if data is None or len(data[0]) == 0:
            return NotFound(response_message["data_not_found"])

        return Ok(response_message["data_found"], data)
    except Exception as e:
        return InternalServerError("searching jumlah_jenis_kendaaran_per_tahun", e)


@sarana_unit_mobil.get('/jumlah_detail_jenis_kendaaran_per_tahun', description="Search jumlah detail jenis kendaaran per tahun")
async def jumlah_detail_jenis_kendaraan_per_tahun(params: QueryParams = Depends()):
    try :
        data = await get_jumlah_detail_jenis_kendaraan_per_tahun()
        if data is None or len(data[0]) == 0:
            return NotFound(response_message["data_not_found"])
        
        if params.return_type == ReturnType.list :
            return Ok(response_message["data_found"], data)

        # Group by tahun
        grouped_data = defaultdict(list)
        for item in data:
            grouped_data[item['tahun']].append(item)

        # Convert grouped_data to a list of lists
        grouped_list = list(grouped_data.values())

        return Ok(response_message["data_found"], grouped_list)
    
    except Exception as e:
        return InternalServerError("searching jumlah_jenis_kendaaran_per_tahun", e)