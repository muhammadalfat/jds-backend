from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from decouple import config

SQLALCHEMY_DATABASE_URL = f"postgresql+psycopg2://{config('DATABASE_USER')}:{config('DATABASE_PASSWORD')}@{config('DATABASE_URL')}"
engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={'options': '-csearch_path={}'.format(config('DATABASE_SCHEMA'))})
engine = engine.execution_options(autocommit=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

# conn = engine.connect().execution_options(autocommit=True)
conn = engine.connect()
