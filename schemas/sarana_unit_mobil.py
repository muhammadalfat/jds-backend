# schemas/user.py
from pydantic import BaseModel, Field
from enum import Enum

# Enum for return type
class ReturnType(str, Enum):
    list = "list"
    object = "object_list"

# Pydantic model for query parameters
class QueryParams(BaseModel):
    return_type: ReturnType = Field(..., description="Choose 'list' or 'object' as the return type")
