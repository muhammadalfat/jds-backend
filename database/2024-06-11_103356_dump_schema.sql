--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2 (Debian 16.2-1.pgdg120+2)
-- Dumped by pg_dump version 16.2 (Debian 16.2-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_table_access_method = heap;

--
-- Name: tb_h_jumlah_sarana_unit_mobil; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_h_jumlah_sarana_unit_mobil (
    id integer NOT NULL,
    kode_provinsi integer,
    nama_provinsi character varying(512),
    bps_kode_kabupaten_kota integer,
    bps_nama_kabupaten_kota character varying(512),
    jenis_kendaraan character varying(512),
    kendaraan character varying(512),
    jumlah_mobil character varying(50),
    satuan character varying(50),
    tahun character varying(5),
    created_by character varying(50) DEFAULT 'system'::character varying,
    created_dt timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_by character varying(50),
    updated_dt timestamp without time zone
);


--
-- Name: tb_h_jumlah_sarana_unit_mobil_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tb_h_jumlah_sarana_unit_mobil_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tb_h_jumlah_sarana_unit_mobil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tb_h_jumlah_sarana_unit_mobil_id_seq OWNED BY public.tb_h_jumlah_sarana_unit_mobil.id;


--
-- Name: tb_h_jumlah_sarana_unit_mobil id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_h_jumlah_sarana_unit_mobil ALTER COLUMN id SET DEFAULT nextval('public.tb_h_jumlah_sarana_unit_mobil_id_seq'::regclass);


--
-- Name: tb_h_jumlah_sarana_unit_mobil tb_h_jumlah_sarana_unit_mobil_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_h_jumlah_sarana_unit_mobil
    ADD CONSTRAINT tb_h_jumlah_sarana_unit_mobil_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

