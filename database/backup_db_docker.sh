# Define the path variable
dump_path="D:\Journey\Jabar-Digital-Service\Pre-requisite\Program\Backend\database"

# Get current date and time
current_datetime=$(date +"%Y-%m-%d_%H%M%S")

# Dump schema (create table & constraint)
docker exec -t main-postgres pg_dump -U postgres --schema-only --no-owner --no-acl --no-tablespaces jabar_digital_service > "${dump_path}\\${current_datetime}_dump_schema.sql"

# Dump data inserts
docker exec -t main-postgres pg_dump -U postgres --data-only --inserts --no-owner --no-acl jabar_digital_service > "${dump_path}\\${current_datetime}_dump_data.sql"
