--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2 (Debian 16.2-1.pgdg120+2)
-- Dumped by pg_dump version 16.2 (Debian 16.2-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: tb_h_jumlah_sarana_unit_mobil; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (1, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR/POMPA', '14', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (2, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT POMPA', '1', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (3, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT PERALATAN', '1', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (4, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL TANGKI', '1', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (5, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL RESCUE', '2', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (6, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL UNIT TANGGA', '2', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (7, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL SNORKLE', '1', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (8, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL M V U', '1', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (9, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL CRANE', '-', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (10, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'MOBIL BINLUH', '1', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (11, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '2', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (12, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL ANGKUTAN PERSONIL', '-', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (13, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL OPERSIONAL (PICK UP T120)', '-', 'UNIT', '2013', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (14, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR/POMPA', '16', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (15, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT POMPA', '1', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (16, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT PERALATAN', '1', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (17, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL TANGKI', '2', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (18, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL RESCUE', '2', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (19, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL UNIT TANGGA', '2', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (20, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL SNORKLE', '1', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (21, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL M V U', '1', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (22, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL CRANE', '-', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (23, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'MOBIL BINLUH', '1', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (24, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '4', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (25, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL ANGKUTAN PERSONIL', '1', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (26, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL OPERSIONAL (PICK UP T120)', '-', 'UNIT', '2014', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (27, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR/POMPA', '16', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (28, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT POMPA', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (29, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT PERALATAN', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (30, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL TANGKI', '2', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (31, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL RESCUE', '2', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (32, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL UNIT TANGGA', '2', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (33, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL SNORKLE', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (34, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL M V U', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (35, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL CRANE', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (36, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'MOBIL BINLUH', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (37, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '5', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (38, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL ANGKUTAN PERSONIL', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (39, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL OPERSIONAL (PICK UP T120)', '1', 'UNIT', '2015', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (40, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR/POMPA', '18', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (41, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT POMPA', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (42, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT PERALATAN', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (43, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL TANGKI', '2', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (44, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL RESCUE', '2', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (45, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL UNIT TANGGA', '2', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (46, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL SNORKLE', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (47, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL M V U', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (48, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL CRANE', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (49, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'MOBIL BINLUH', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (50, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '6', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (51, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL ANGKUTAN PERSONIL', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (52, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL OPERSIONAL (PICK UP T120)', '1', 'UNIT', '2016', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (53, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR/POMPA', '19', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (54, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT POMPA', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (55, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT PERALATAN', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (56, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL TANGKI', '2', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (57, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL RESCUE', '2', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (58, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL UNIT TANGGA', '2', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (59, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL SNORKLE', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (60, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL M V U', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (61, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL CRANE', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (62, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'MOBIL BINLUH', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (63, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '9', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (64, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL ANGKUTAN PERSONIL', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (65, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL OPERSIONAL (PICK UP T120)', '1', 'UNIT', '2017', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (66, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR', '12', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (67, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR', '3', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (68, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR', '2', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (69, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (70, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL PANCAR', '2', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (71, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT POMPA', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (72, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL QUICK RESPONSE UNIT PERALATAN', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (73, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'MOBIL TANGKI', '2', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (74, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL RESCUE', '2', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (75, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL UNIT TANGGA', '2', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (76, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL SNORKLE', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (77, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBILÂ  M V U', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (78, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL CRANE', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (79, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'MOBIL BINLUH', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (80, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '11', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (81, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (82, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (83, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (84, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (85, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL ANGKUTAN PERSONIL', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (86, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL OPERSIONAL (PICK UP T120)', '1', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (87, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOTOR TRAIL', '5', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (88, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOTOR BINLUH', '6', 'UNIT', '2019', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (89, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '6', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (90, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '5', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (91, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '3', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (92, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '2', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (93, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '4', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (94, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'TANGKI', '3', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (95, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'QUICK RESPONSE', '2', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (96, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL RESCUE', '2', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (97, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MOBIL TANGGA', '2', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (98, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'SNORKLE', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (99, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'MVU', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (100, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'RESCUE', 'CRANE', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (101, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'RESCUE', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (102, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'PANCAR', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (103, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENCEGAHAN', 'BUS', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (104, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '7', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (105, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (106, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (107, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (108, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOBIL KOMANDO', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (109, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOTOR TRAIL', '5', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (110, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'MOTOR PENCEGAHAN', '6', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (111, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'ANGKUTAN PERSONIL', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (112, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'OPERASIONAL BENGKEL', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (113, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENDUKUNG', 'OPERASIONAL BENGKEL', '1', 'UNIT', '2020', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (114, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '7', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (115, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '3', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (116, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '3', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (117, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '2', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (118, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR', '3', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (119, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'QUICK RESPONSE POMPA', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (120, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENYELAMATAN', 'QUICK RESPONSE PERALATAN', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (121, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'TANGKI', '3', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (122, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENYELAMATAN', 'RESCUE', '2', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (123, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENYELAMATAN', 'TANGGA', '2', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (124, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENYELAMATAN', 'SNORKLE', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (125, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENYELAMATAN', 'MVU', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (126, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENYELAMATAN', 'CRANE', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (127, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PENYELAMATAN', 'RESCUE BINLUH', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (128, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'PEMADAMAN', 'PANCAR BINLUH', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (129, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'ANGKUTAN PERSONIL', 'BUS DAMKAR', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (130, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'KOMANDO', 'KOMANDO', '8', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (131, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'KOMANDO', 'KOMANDO', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (132, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'KOMANDO', 'KOMANDO', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (133, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'KOMANDO', 'KOMANDO', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (134, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'KOMANDO', 'KOMANDO', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (135, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'ANGKUTAN OPERASIONAL', 'MOBIL ANGKUTAN OPERASIONAL', '1', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);
INSERT INTO public.tb_h_jumlah_sarana_unit_mobil VALUES (136, 32, 'JAWA BARAT', 3273, 'KOTA BANDUNG', 'OPERASIONAL BENGKEL', 'MOBIL OPERASIONAL BENGKEL', '2', 'UNIT', '2021', 'system', '2024-06-11 10:31:56.238232', NULL, NULL);


--
-- Name: tb_h_jumlah_sarana_unit_mobil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tb_h_jumlah_sarana_unit_mobil_id_seq', 1000, false);


--
-- PostgreSQL database dump complete
--

