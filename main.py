import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routes.sarana_unit_mobil import sarana_unit_mobil

app = FastAPI(
        title="Sarana Unit Mobil Pemadam dan Bencana Kota Bandung API Documentation", 
        description="<h3>API Documentation for Sarana Unit Mobil Pemadam dan Bencana Kota Bandung.</h3>Created by Muhammad Alfat Furqon",
        docs_url="/swagger", 
        contact={"email":"muhammadalfat.ma@gmail.com"},
        debug=True
    )


def cors_headers(app):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_methods=["*"],
        allow_headers=["*"],
        allow_credentials=True,
    )
    return app

# Basic
app.include_router(sarana_unit_mobil, prefix='/api/v1/sarana_unit_mobil', tags=["sarana_unit_mobil"])


app = cors_headers(app)


@app.get("/", tags=["Root"])
async def root():
    return {
        "message": "API Sarana Unit Mobil Pemadam dan Bencana Kota Bandung",
        "version": "1.0.0"
    }

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=3004)
