from fastapi import Depends, HTTPException, Security
from fastapi.security.api_key import APIKeyHeader
from decouple import config
from utilities.messageutil import response_message

# Instantiate the configuration with your valid API keys
api_key_header = APIKeyHeader(name="X-API-KEY", auto_error=False)

def verify_api_key(api_key_header: str = Security(api_key_header)):
    if api_key_header != config('API_KEY'):
        raise HTTPException(status_code=403, detail=response_message["invalid_token"])
    
    return api_key_header